export const anime_episodes = [
    {
        id: 13,
        episode_number: 1,
        episode_name: "The Revival of the Long-established Classic Literature Club",
        episode_thumbnail: "/images/episodes/hyouka/Hyouka-ep-1.png",
        episode_length: 1254
    },
    {
        id: 14,
        episode_number: 2,
        episode_name: "The Activities of the Esteemed Classic Literature Club",
        episode_thumbnail: "/images/episodes/hyouka/Hyouka-ep-2.png",
        episode_length: 1451
    },
    {
        id: 15,
        episode_number: 3,
        episode_name: "The Descendants of the Classic Literature Club",
        episode_thumbnail: "/images/episodes/hyouka/Hyouka-ep-3.png",
        episode_length: 1257
    },
    {
        id: 16,
        episode_number: 4,
        episode_name: "The Old Days of the Glorious Classic Literature Club",
        episode_thumbnail: "/images/episodes/hyouka/Hyouka-ep-4.png",
        episode_length: 1405
    },
    {
        id: 17,
        episode_number: 5,
        episode_name: "The Truth of the Historic Classic Literature Club",
        episode_thumbnail: "/images/episodes/hyouka/Hyouka-ep-5.png",
        episode_length: 1382
    },
    {
        id: 18,
        episode_number: 6,
        episode_name: "To Commit a Grave Sin",
        episode_thumbnail: "/images/episodes/hyouka/Hyouka-ep-6.png",
        episode_length: 1436
    },
    {
        id: 13,
        episode_number: 7,
        episode_name: "The Revival of the Long-established Classic Literature Club",
        episode_thumbnail: "https://reallifeanime.files.wordpress.com/2015/08/hyouka-episode-1-classics-club.png",
        episode_length: 1254
    },
    {
        id: 14,
        episode_number: 8,
        episode_name: "  The Activities of the Esteemed Classic Literature Club",
        episode_thumbnail: "/images/episodes/hyouka/Hyouka-ep-2.png",
        episode_length: 1451
    },
    {
        id: 15,
        episode_number: 9,
        episode_name: "The Descendants of the Classic Literature Club",
        episode_thumbnail: "/images/episodes/hyouka/Hyouka-ep-3.png",
        episode_length: 1257
    },
    {
        id: 16,
        episode_number: 10,
        episode_name: "The Old Days of the Glorious Classic Literature Club",
        episode_thumbnail: "/images/episodes/hyouka/Hyouka-ep-4.png",
        episode_length: 1405
    },
    {
        id: 17,
        episode_number: 11,
        episode_name: "The Truth of the Historic Classic Literature Club",
        episode_thumbnail: "/images/episodes/hyouka/Hyouka-ep-5.png",
        episode_length: 1382
    },
    {
        id: 18,
        episode_number: 12,
        episode_name: "To Commit a Grave Sin",
        episode_thumbnail: "/images/episodes/hyouka/Hyouka-ep-6.png",
        episode_length: 1436
    }
];
