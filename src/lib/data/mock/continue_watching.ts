export const continue_watching = [
	{
		name: "Oshi no ko",
		banner: "/images/Oshinoko.jpg",
		current_episode: 4,
		episodes_count: 12,
		time_watched: 1356
	},
	{
		name: "Kono Subarashii Sekai ni Bakuen wo!",
		banner: "/images/KonoSubarashii.jpg",
		current_episode: 7,
		episodes_count: 25,
		time_watched: 1127
	}
]