<div align="center">

  <h2 align="center">ANIMECORE V3 UI</h2>

  <p align="center">
   <b>A modern anime streaming platform under CoreProject
     <br>
   This is the V3 UI for <a href="https://github.com/baseplate-admin/CoreProject/">CoreProject</a> / <a href="https://github.com/Tokito69/CoreProject-V3-UI/">AnimeCore<a/>
     <br>
     check <a href="https://coreproject.moe/anime/">v2 UI</a>
    <br><br>
    <a href="https://github.com/baseplate-admin/CoreProject"><strong>Explore the docs »</strong></a>
  </p>
</div>

<p align="center">
  <a href="https://github.com/Tokito69/CoreProject-V3-UI/graphs/contributors" alt="Contributors">
    <img src="https://img.shields.io/github/contributors/Tokito69/CoreProject-V3-UI.svg?style=for-the-badge" >
  </a>
  <a href="https://github.com/Tokito69/CoreProject-V3-UI/network/members" alt="Forks">
    <img src="https://img.shields.io/github/forks/Tokito69/CoreProject-V3-UI.svg?style=for-the-badge">
  </a>
  <a href="https://github.com/Tokito69/CoreProject-V3-UI/issues" alt="Issues">
    <img src="https://img.shields.io/github/issues/Tokito69/CoreProject-V3-UI.svg?style=for-the-badge">
  </a>
  <a href="https://github.com/Tokito69/CoreProject-V3-UI/blob/v2/LICENSE" alt="License - AGPL-3.0">
    <img src="https://img.shields.io/github/license/Tokito69/CoreProject-V3-UI.svg?style=for-the-badge">
  </a>

  <img alt="GitHub code size in bytes" src="https://img.shields.io/github/languages/code-size/Tokito69/CoreProject-V3-UI?style=for-the-badge">
  <img alt="Lines of code" src="https://img.shields.io/tokei/lines/github/Tokito69/CoreProject-V3-UI?style=for-the-badge">
  <a href='https://discord.gg/7AraSmKqnN'><img alt="Discord" src="https://img.shields.io/discord/1039894823626362931?style=for-the-badge"></a>
</p>
    
## Screenshots

<p float="left">
  <i>Click on the images to open full view in new tab</i>
  <br>
  <br>
  <img src="https://imgbox.io/ib/mZwPoOMD11.png" alt="Home Page Mockup" width=49%>
  <img src="https://imgbox.io/ib/OUbB4rsyCh.png" alt="Anime Info Page Mockup" width=49%>
  <img src="https://imgbox.io/ib/zPypve2smU.png" alt="Register Page Mockup" width=49%>
  <img src="https://imgbox.io/ib/22dteHTHrH.png" alt="Search Panel Mockup" width=49%>

## Contributing

-   If you have a suggestion/idea that would make this project better, please create a pull request. All pull requests will be reviewed by us, and adjusted.

-   You can also [open a new issue](https://github.com/Tokito69/CoreProject-V3-UI/issues/new/choose) or [help us with an existing one](https://github.com/Tokito69/CoreProject-V3-UI/issues).

Other than that, you can also help the project by giving it a star! Your help is extremely appreciated :)

## License

Distributed under the AGPL-3.0 License. See [`LICENSE`](https://github.com/Tokito69/CoreProject-V3-UI/blob/v2/LICENSE) for more information.
